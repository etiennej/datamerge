#!/bin/bash
## DataMerge
# <img align="right" width="125" height="125" src="junk/datamergelogo.png">
# DataMerge is a linux command line which allows to merge a number of datafiles
# organised in columns indexed in a one-dimensional way into a single dataset.
# It can handle files of data tabulated on different grids, by using
# interpolation methods. E.g., data recorded every 10 seconds and data recorded
# every 15 seconds can be interpolated into either grid. Interpolation can be
# linear between adjacent points, or use a least-square approximation on a
# moving window. It also allows to process columns individually, perform
# cross-column calculations or change the unit in which quantities are
# expressed. Several other methods also allow to perform cross-row calculations,
# such as finite differences or normalisation by an automatically detected 
# entry row.

### (C) J Etienne 2012-2022
# DataMerge is released under [GPL 2 licence](LICENSE).
# DataMerge logo CC-BY 4.0 based on original work by [Delapouite](https://delapouite.com/)

#// Copyright (C) 2022 Jocelyn Etienne <Jocelyn.Etienne@ujf-grenoble.fr>
#//
#// This file is part of the DataMerge software.
#//
#// DataMerge is free software; you can redistribute it and/or modify
#// it under the terms of the GNU General Public License as published by
#// the Free Software Foundation; either version 2 of the License, or
#// (at your option) any later version.
#//
#// DataMerge is distributed in the hope that it will be useful,
#// but WITHOUT ANY WARRANTY; without even the implied warranty of
#// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#// GNU General Public License for more details.
#//
#// You can find a copy of the GNU General Public License at
#//    http://www.gnu.org/copyleft/gpl.html
#// if this is not possible for you, write to the Free Software
#// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA




### Generate initial datasets

# Some `bash` first to generate a time grid (files also provided in `git`)

NMAX=40
RATE=0.01
seq 0 3 ${NMAX} | tr " " "\n" > example/sampling_3.tsv
seq 1 7 ${NMAX} | tr " " "\n" > example/sampling_7.tsv

# Then use `datamerge` to generate data from those time grids. 

datamerge --force --separator tab \
	--reference example/sampling_3.tsv 'LINENUMBER+1' time \
	--constant rate $RATE  \
	--constant pi '3.14159265' \
	--output example/dataset_sampling_3.tsv 'line' 'time sin(2*pi*time*rate) exp(time*rate)'

# Complex mathematical expressions can be evaluated at output time, using any of the variables
# that have been defined before when reading the input files. The evaluation of these expressions
# is delegated to the [GiNaC](https://www.ginac.de/) library.

# Note that constants can be declared for further use.
# `--force` option overwrites existing output file.
# `--separator` is needed when it cannot be guessed from input file.
# Several option names can be abbreviated.

# We now generate another datafile with a different time grid:

datamerge -f -s tab \
	-r example/sampling_7.tsv 'LINENUMBER+1' time \
	--constant rate $RATE  \
	--constant pi '3.14159265' \
	-o example/dataset_sampling_7.tsv 'line' 'time cos(2*pi*time*rate) exp(time*rate)'

### Interpolate between datasets
# The time grid for output is the one of the file called by the `--reference` option.
# As many additionnal `--input` files as desired can be used, linear interpolation is performed.
# `na` is inserted if values cannot be interpolated.
# Field names have to be unique.

datamerge -f \
	--reference example/dataset_sampling_3.tsv 'time_sampling_3' \
		'line_sampling_3 time_sampling_3 sinusoidal exponential' \
	--input example/dataset_sampling_7.tsv 'time_sampling_7' \
		'line_sampling_7 time_sampling_7 cosinusoidal exponential_sampling_7'	\
	-o example/linear_interpolation.tsv 'time' \
		'sinusoidal cosinusoidal exponential exponential-exponential_sampling_7 sinusoidal^2+cosinusoidal^2'

### Select values
# Comparison operators `<`, `<=`, `==`, `!=`,... allow to filter entries in reference and any input file according to values
# of any column. The option `--filter_epsilon` allows to set a tolerance (default is 0).
# Note the usage of `#` as a blackhole to ignore some columns.

datamerge -f \
	-r example/dataset_sampling_7.tsv 'time' '# time cosinusoidal<=0 exponential' \
	-o example/dataset_selection.tsv 't' 'cosinusoidal exponential'

### Normalise data by value reached at some timepoint
# We now want to detect when one of the variables reaches a given threshold,
# and, for each column, use the corresponding entry to normalise all values in
# the dataset. This can be done by a combination of a test and a special
# interpolation called `{previous}`, which returns the latest entry that was
# matching the test.

datamerge -f \
	-r example/dataset_sampling_3.tsv 'time' \
		'# time>0 sinusoidal exponential' \
	-i example/dataset_sampling_3.tsv 'time_thresh{previous}' \
		'# time_thresh sinusoidal_at_thresh exponential_thresh<1.1'	\
	-o example/normalised.tsv 't' 'sinusoidal/sinusoidal_at_thresh time_thresh exponential/exponential_thresh' 

# However, we may want to normalise also retrospectively the data that was present before the last entry matching the test.
# For this, we replace `time_thresh` by the value `0`: if `time` is always positive, then the `{previous}` interpolation operator
# will always return the last match of the test in the whole datafile.

datamerge -f \
        -r example/dataset_sampling_3.tsv 'time' \
                '# time>0 sinusoidal exponential' \
        -i example/dataset_sampling_3.tsv '0{previous}' \
                '# time_thresh sinusoidal_at_thresh exponential_thresh<1.1'     \
        -o example/all_normalised.tsv 't' 'sinusoidal/sinusoidal_at_thresh time_thresh exponential/exponential_thresh'

# :exclamation:  The result of the `{previous}` keyword is guaranteed only when using `C++-11`-compliant compilers. Checks are in place to verify this. 

### Multiple columns bulk management

# First, generate some data with `bash` if file (provided in the git) is not found

NCOL=10
if [ ! -e example/dataset_random.tsv ]
then
    echo "# matrix of random numbers in [-16384,16383], $NMAX x $NCOL" > example/dataset_random.tsv
    for i in $(seq 1 $NMAX)
    do
      echo -n $i >> example/dataset_random.tsv
      for j in $(seq 1 $NCOL)
      do
	echo -n " $(( RANDOM - 16384 ))" >> example/dataset_random.tsv
      done
      echo >> example/dataset_random.tsv
    done
fi

# Multiple columns bulk are managed with `variable[number]`, or, for output only, `variable[from:to]` or `variable[from:to:step]` syntaxes.
# Note here that `phase := line*0.1` can be used as a keyword in output columns.

datamerge -f --constant MAX 32768. --constant SNR 5. \
	-r example/dataset_random.tsv "line*0.1" "line r[10]" \
	-o example/multicolum.tsv 'phase' "sin(phase)+r__1/(MAX*SNR) cos(phase)+r__0/(MAX*SNR) r[5] r[0:4]/MAX r[5:9]-r[9:5:-1]"

# In `--input` or `--reference`, only the number of columns can be provided. In `--output`, one can use a range of values
# in the form `<first_index>:<last_index>`, and optionnally add a step size, `<first_index>:<last_index>:<step>`. One
# can also simply use a number `<n>` corresponding to the `n` first columns (`r[5]` in the example is equivalent to `r[0:4]`).
# Note that the table variables appear with double `_` and their index, and can be accessed in that way individually (`r__0`
# and `r__1` in the example).

# Some operations can be performed over a range of columns. E.g.:

datamerge -f \
	-r example/dataset_random.tsv 'line*0.1' 'line r[10]' \
	-o example/dataset_sum.tsv t 'SUM{r[10]}/STAT_N{r[10]} SUM{r[1:3]}-(r__1+r__2+r__3)'

# Note the `{}` brackets. There is no possibility of nesting such operators, several runs of DataMerge are necessary
# if such operation is desired. If some values are not available, the result is `na` unless the `--cumulate_through`
# flag is set, in which case unavailable values are ignored.

datamerge -f --cumulate_through \
	-r example/dataset_random.tsv 'line*0.1' 'line r[10]' \
	-o example/dataset_prod.tsv t 'STAT_N{r[999]} PROD{r[8:666]}-r__8*r__9' 

# Available operators can be obtained with

datamerge --dump_multicol_operators > example/list_of_operators.txt

# `STAT_N` returns the number of values which are not undefined.

### Multicolumn statistics the GiNaC way

# Some additional operations can be made on multiple columns *and* can be nested, namely: `min`, `max`, `mean` and `median`. Because of the way DataMerge handles 
# expressions (using the [GiNaC](https://www.ginac.de/) library), it is needed to specify the number of variables over which the operator is applied, and give
# each explicitely (no bulk processing with the `[]` operator is possible), e.g.: `max3(a,b,c)`. An additional operator, `stat_n`,
# returns the number of its arguments which have a value for the given entry, i.e. `stat_n3(a,b,c)` returns 3 if `a`, `b` and `c`
# have a value, or less than 3 if any or several of them are not available (`na`).

datamerge -f \
	-r example/linear_interpolation.tsv 't' \
		't sinusoidal cosinusoidal exponential # #' \
	-o example/statistics.tsv 'time' \
		'min4(sinusoidal,cosinusoidal,exponential,1/exponential) max4(sinusoidal,cosinusoidal,exponential,1/exponential)  mean4(sinusoidal,cosinusoidal,exponential,1/exponential) stat_n4(sinusoidal,cosinusoidal,exponential,1/exponential)'

### Calculate finite differences
# Note multiple reading of the same data but with a time shift.

datamerge -f \
	--constant dt 3 \
	-r example/dataset_sampling_3.tsv 'time_plus-dt/2' \
		'# time_plus sinusoidal_plus exponential_plus' \
	-i example/dataset_sampling_3.tsv 'time_minus+dt/2' \
		'# time_minus sinusoidal_minus exponential_minus' \
	-o example/dataset_finite_diff.tsv 'time' \
		'(sinusoidal_plus-sinusoidal_minus)/(time_plus-time_minus) (exponential_plus-exponential_minus)/(time_plus-time_minus)'

# Note that the points which are not within the range of the grid they are interpolated from are not available (`na`), this is
# the case here with the first point shown, since there is no value at time `-3` that would allow to calculate the finite difference
# at `-1.5`. This point is shown because `time_plus-dt/2` evaluates to `-1.5` on the first line of the reference file. It is possible
# to filter it out:

datamerge -f \
	--constant dt 3 \
	-r example/dataset_sampling_3.tsv 'time_plus-dt/2' \
		'# time_plus>0 sinusoidal_plus exponential_plus' \
	-i example/dataset_sampling_3.tsv 'time_minus+dt/2' \
		'# time_minus sinusoidal_minus exponential_minus' \
	-o example/dataset_finite_diff_filtered.tsv 'time' \
		'(sinusoidal_plus-sinusoidal_minus)/(time_plus-time_minus) (exponential_plus-exponential_minus)/(time_plus-time_minus)'

### Cumulate values to calculate series, approximate integrals,...

# For this, use the symbols `+=` or `*=` in `--output` columns, which defines *cumulator* variables.

datamerge -f --constant pi 3.14159265 \
	-r example/dataset_sampling_3.tsv N "N" \
	-o example/dataset_cumulate.tsv n "s+=n s s-n*(n-1)/2 factorial*=(n+1) factorial 1-sqrt(2*pi*n)*(n/exp(1))^n/factorial"

# Note that in the column where the "cumulator" variable is calculated, the value printed in a row is the one
# *after* calculation has taken place (left-hand side of column definition). In all other columns, the cumulator
# variable name can be called and contains the value *calculated in the previous row*. This is symbolised by the
# "__+" subscript.

# Additive cumulators are initialised to 0, multiplicative ones to 1. 
# If data is missing in a row and cannot be cumulated, the column is `na` after that row, unless the option `--cumulate_through` is set.

### Interpolate between datasets with LOESS 
# See [Wikipedia](https://en.wikipedia.org/wiki/Local_regression).
# Note that this is more appropriate for high-sampling large-noise data.
# Note also that this example shows that additional columns in file are ignored. 
# You probably have also noticed that column headers in the file are not used, but are rather
# always re-defined in the command line options.

datamerge -f --bandwidth 0.8 --skip_header 1 \
	-r example/dataset_sampling_3.tsv "2*acos(-1.)*time_sampling_3*${RATE}" \
		'# time_sampling_3>0 sinusoidal' \
	-i example/multicolum.tsv 'phase_input' \
		'phase_input noisy_sin' \
	-i example/multicolum.tsv 'phase_input_lsq{loess}' \
		'phase_input_lsq noisy_sin_lsq' \
	-o example/loess_interpolation.tsv 'phase_output' \
		'sinusoidal noisy_sin_lsq noisy_sin ((noisy_sin_lsq-sinusoidal)/sinusoidal) ((noisy_sin-sinusoidal)/sinusoidal)' 

# Note that the points for which the time-window of LOESS is incomplete are not available (`na`)

### Calculate derivative with LOESS
# A dash `'` can be used to denote the derivative of any variable that was read as a LOESS-interpolated input.
# Note that alternatively you can use the long format `_derivative`.
# In the example below the reference file is used only to provide the time grid.

datamerge -f --bandwidth 0.8 \
	-r example/multicolum.tsv 'time_orig' \
		'time_orig' \
	-i example/multicolum.tsv 'time_leastsquares{loess}' \
		'time_leastsquares sinusoidal cosinusoidal' \
	-o example/loess_derivation.tsv 'time' \
		"sinusoidal sinusoidal' cosinusoidal -cosinusoidal_derivative abs(sinusoidal+cosinusoidal')"

### Non-regression test
# If differences are shown in file `example/diff` when you run this example,
# DataMerge may be broken. Check [doi: 10.5281/zenodo.5911337](https://dx.doi.org/10.5281/zenodo.5911337) 
# for the current version and recompile using an up-to-date C++-11 compliant
# compiler.  If problem persists, please contact the authors.

echo Non-regression test fails if any difference is listed below > example/diff
for LOC in example/*.tsv
do
 REF=$(basename $LOC)
 diff -q $LOC example/reference_results/$REF >> example/diff
done
echo End of non-regression test >> example/diff

# I hope you enjoy DataMerge! Don't hesitate to share usage cases. Contributions welcome.
