LDLIBS = -lginac -lcln -lgsl -lgslcblas
CC=g++

default: datamerge

clean: 
	rm datamerge *.o

datamerge.o: datamerge.cc maths/mean.icc maths/min.icc maths/max.icc maths/median.icc
