//   DataMerge
//
// Merge data tabulated on different 1D grids e.g. time points
//

/// Copyright (C) 2012-2022 Jocelyn Etienne <Jocelyn.Etienne@ujf-grenoble.fr>
///
/// DataMerge is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// DataMerge is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You can find a copy of the GNU General Public License at
///    http://www.gnu.org/copyleft/gpl.html
/// if this is not possible for you, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <fstream>
#include <ginac/ginac.h>
#include <map>
#include <list>
#include <string>
#include <limits>
#include <sys/stat.h>
#include <gsl/gsl_fit.h>
#include <regex>
#include <numeric>
using namespace std;
using namespace GiNaC;

#define VERSION "1.0.10"

# define fatal_macro(message) \
        { std::cerr << "fatal(" << __FILE__ << "," << __LINE__ << "): " << message << std::endl; exit(1); }
# define error_macro(message) \
        fatal_macro(message)
# define warning_macro(message) \
        { std::cerr << "warning(" << __FILE__ << "," << __LINE__ << "): " << message << std::endl; }
#  define check_macro(ok_condition, message) \
        { if (!(ok_condition)) fatal_macro(message); }
#  define trace_mac(i,message) \
    { if (i<=TRACE_LEVEL) cerr << "trace(" << __FILE__ << "," << __LINE__ << "): " << message << endl; }

#define TRACE_LEVEL 5

const string notavailable="na";
const symbol NA("na");

typedef map<string, double> record_t;
typedef multimap<double, record_t> datafile_t;
typedef list<datafile_t> dataset_t;
typedef list<string> fieldnames_t;
typedef list<pair<size_t, string> > cumulator_t;

// GiNaC-handled maths

//Integer functions
#include "maths/integers.icc"

//Statistical functions
#include "maths/mean.icc"
#include "maths/median.icc"
#include "maths/min.icc"
#include "maths/max.icc"
#include "maths/stat_n.icc"

// Multicolumn operators
const fieldnames_t multi_operators = fieldnames_t({"SUM","PROD","MEAN","MEDIAN","VAR","STDDEV","BIASED_VAR","BIASED_STDDEV","STAT_N"});

double median (list<double> vals)
 {
   size_t n = vals.size();
   size_t m = n/2;
   vector<double> tmp(m+1);
   partial_sort_copy(vals.begin(), vals.end(), tmp.begin(), tmp.end());

    if (n % 2 == 0) {
        return (tmp[m-1] + tmp[m]) / 2;
    }
    return tmp[m];
}

ex
multi_op_manage(string moperator, list<double> vals)
 {
   regex index("__[0-9]+");
   moperator=regex_replace(moperator,index,"");
   auto ival = vals.begin();
   auto eval = vals.end();
   if (moperator=="SUM") 
	return accumulate(ival, eval, 0.);
   else if (moperator=="MEAN") 
	return accumulate(ival, eval, 0.)/vals.size();
   else if (moperator=="PROD") 
	return accumulate(ival, eval, 1., std::multiplies<double>());
   else if (moperator=="STAT_N") 
        return vals.size();
   else if (moperator=="MEDIAN") 
	return median(vals);
   else if (moperator=="STDDEV"||moperator=="VAR"||moperator=="BIASED_STDDEV"||moperator=="BIASED_VAR")
      {
	vector<double> diff(vals.size());
        double mean=accumulate(ival, eval, 0.)/vals.size();
   	transform(ival, eval, diff.begin(), [mean](double x) { return x - mean; });
	double sq_sum = inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
	if (moperator=="VAR")    	return     (sq_sum / (vals.size()-1));
	else if (moperator=="STDDEV") 	return sqrt(sq_sum / (vals.size()-1));
	else if (moperator=="BIASED_VAR")    	return     (sq_sum / (vals.size()));
	else if (moperator=="BIASED_STDDEV") 	return sqrt(sq_sum / (vals.size()));
	else error_macro("Unknown multicolumn operator " << moperator );
      }
   else error_macro("Unknown multicolumn operator " << moperator );
 }

// Definitions for GiNaC : 
//  * working with integers
DECLARE_FUNCTION_2P(div)
static ex div_evalf(const ex& x, const ex& y)
 {
    if (is_a<numeric>(x) && is_a<numeric>(y))
     { 
       if (ex_to<numeric>(x).to_double()!=numeric_limits<double>::max()
    	   && ex_to<numeric>(y).to_double()!=numeric_limits<double>::max())
       	 {
	    long int ix=round(ex_to<numeric>(x).to_double());
	    long int iy=round(ex_to<numeric>(y).to_double());
       	    return iquo(ix, iy); 
	 }
	else return numeric_limits<double>::max(); 
     }
    else
     {
       error_macro("Integer division defined only for numerical values");
       return x;
     }
 }
REGISTER_FUNCTION(div, evalf_func(div_evalf))

DECLARE_FUNCTION_2P(mod)
static ex mod_evalf(const ex& x, const ex& y)
 {
    if (is_a<numeric>(x) && is_a<numeric>(y))
     { 
       	if (ex_to<numeric>(x).to_double()!=numeric_limits<double>::max()
    	   && ex_to<numeric>(y).to_double()!=numeric_limits<double>::max())
       	 {  
	    long int ix=round(ex_to<numeric>(x).to_double());
	    long int iy=round(ex_to<numeric>(y).to_double());
       	    //cerr << "?" << ix << " " << iy <<endl;
	    return irem(ix, iy); 
	 }
	else return numeric_limits<double>::max(); 
     }
    else
     {
       error_macro("Integer division defined only for numerical values");
       return x;
     }
 }
REGISTER_FUNCTION(mod, evalf_func(mod_evalf))

// vectors
DECLARE_FUNCTION_4P(dist)
static ex dist_evalf(const ex& x1, const ex& y1, const ex& x2, const ex& y2)
 {
 	return sqrt(pow(x1-x2,2)+pow(y1-y2,2));
 }
REGISTER_FUNCTION(dist, eval_func(dist_evalf))

// Specific
DECLARE_FUNCTION_12P(atef_h)
static ex atef_h_evalf(const ex& x1, const ex& y1, const ex& x2, const ex& y2,
	const ex& x3, const ex& y3, const ex& x4, const ex& y4,
	const ex& x5, const ex& y5, const ex& x6, const ex& y6)
 {
    	ex nx = - (y2-y1) - (y6-y6);
    	ex ny = + (x2-x1) + (x6-x6);
	nx = nx / sqrt(pow(nx,2)+pow(ny,2));
	ny = ny / sqrt(pow(nx,2)+pow(ny,2));
 	return abs( nx*(x5-x1 + x6-x2) + ny*(y5-y1 + y6-y2) )/4; 
 }
REGISTER_FUNCTION(atef_h, eval_func(atef_h_evalf))


bool debug=false;

struct parameters 
 {
public:
   parameters() : fitting("linear"), Tsmooth(0), testepsilon(0)
    {
    	varmin=numeric_limits<double>::lowest(); 
    	varmax=numeric_limits<double>::max(); 
    }

   string fitting;
   double Tsmooth;
   double varmin;
   double varmax;
   double testepsilon;
 };

bool file_exists(const std::string& filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1)
    {
        return true;
    }
    return false;
}

inline bool should_cumul(cumulator_t c, size_t i) 
{
    if (c.size()==0) return false;
    return ((*c.begin()).first==i);
}

enum test_enum { lt, le, eq, ge, gt, ne };
test_enum label_test(const string& name) {
    if (name=="<") return test_enum::lt;
    if (name=="<=") return test_enum::le;
    if (name=="==") return test_enum::eq;
    if (name=="!=") return test_enum::ne;
    if (name==">=") return test_enum::ge;
    if (name==">") return test_enum::gt;
    return test_enum::eq;
 }
/* ginac::compare doesn't seem to work as described
bool perform_test(test_enum t, ex e) {
    int c=e.compare(0);
    cerr << "TEST RES " << e << c << endl;
    switch (t) {
    	case test_enum::lt : return (c<0); break;
    	case test_enum::le : return (c<=0); break;
    	case test_enum::eq : return (c==0); break;
    	case test_enum::ge : return (c>=0); break;
    	case test_enum::gt : return (c>0); break;
     }
    return false;
}
*/
bool perform_test(test_enum t, ex e, double testepsilon=0) {
    double c=ex_to<numeric>(e).to_double();
    if (c==numeric_limits<double>::max()) return false;
    switch (t) {
    	case test_enum::lt : return (c<testepsilon); break;
    	case test_enum::le : return (c<=testepsilon); break;
    	case test_enum::eq : return (abs(c)<=testepsilon); break;
    	case test_enum::ne : return (abs(c)>=testepsilon); break;
    	case test_enum::ge : return (c>=-testepsilon); break;
    	case test_enum::gt : return (c>-testepsilon); break;
     }
    return false;
}
void
read_data (const string& filename, const string& variable, const string& columns, const string& opt,
	datafile_t& data, symtab& all_vars_table, 
	const parameters& par,  string separator="", size_t skip_lines_in_file=0, bool skip_incomplete=false)
 {
    if (debug) cerr << "{";
    symtab table(all_vars_table); // all variables already known can be used
    istringstream isscol(columns);
    string col;
    fieldnames_t cols;
    isscol >> col;
    string valid_test="1==1";
    list<ex*> valid_tests;
    list<test_enum> valid_tests_nature;
    while (isscol)
     {
        size_t idx;
	// determine if there is a filter associated with this column
	string test_nature="";
	if ((idx=col.find("<",0))!=string::npos || (idx=col.find(">",0))!=string::npos )
	 {
	   test_nature=col.substr(idx,1);
	   size_t idx2=col.find("=",idx+1);
	   if(idx2==idx+1) test_nature+="="; else idx2=idx; 
	   valid_test=col.substr(idx2+1,string::npos);
	   col=col.substr(0,idx);
	 }
	else if ((idx=col.find("!",0))!=string::npos)
	 {
	   size_t idx2=col.find("=",idx+1);
	   check_macro(idx2==idx+1,"Syntax of nonequality filter 'variable!=value', found " << col);
	   test_nature="==";
	   valid_test=col.substr(idx+2,string::npos);
	   col=col.substr(0,idx);
	 } 
	else if ((idx=col.find("=",0))!=string::npos)
	 {
	   size_t idx2=col.find("=",idx+1);
	   check_macro(idx2==idx+1,"Syntax of equality filter 'variable==value', found " << col);
	   test_nature="==";
	   valid_test=col.substr(idx+2,string::npos);
	   col=col.substr(0,idx);
	 }
	else
	 {
	   valid_test="";
	 }
	// determine if this variable is a multicolumn one
	if (col.find("'")!=string::npos) error_macro("Symbol ' is reserved for the derivatives of variables in the output, please change the name of variable " << col << " in input file " << filename); 
	if ((idx=col.find("[",0))!=string::npos)
	 {
            check_macro(valid_test=="","Multicolumn variables cannot be filtered on");		
	    string multicol_base = col.substr(0,idx);
	    size_t idx2=col.find("]",idx+1);
            check_macro(idx2!=string::npos,"Multicolumn syntax: " << multicol_base << "[<number_of_columns>]");		
	    regex reg("[^[:digit:]]");
	    bool idxND=regex_search(col.substr(idx+1,idx2-idx-1),reg);
            check_macro(!idxND,"Multicolumn syntax in --input and --reference statement: " << multicol_base << "[<number_of_columns>]");		
	    size_t number_of_columns = 0;
	    check_macro (1 == sscanf((col.substr(idx+1,idx2-idx-1)).c_str(), "%zu", &number_of_columns),
	    			"Multicolumn syntax: " << multicol_base << "[<number_of_columns>]"
	    			<< " found invalid " << col.substr(idx+1,idx2) << " for number_of_columns.");
	    for (size_t imcol=0; imcol < number_of_columns; imcol++)
	     {
		string coli=(multicol_base+"__"+to_string(imcol));
	     	symbol* symbcol = new symbol(coli);
		table[coli]=*symbcol;
		check_macro(all_vars_table.find(coli)==all_vars_table.end(),
			"Name conflict: " << coli << " already declared");
		all_vars_table[coli]=*symbcol;
		if (opt.find("loess")!=string::npos) 
		 {
			symbol* symbder = new symbol(coli+"_derivative");
			all_vars_table[coli+"_derivative"]=*symbder;
		 }
		cols.push_back(coli);
	     }
	 }
	else
	 { // not a multicolumn variable
	    symbol* symbcol = new symbol(col);
	    check_macro(col!="0" && col!="1" && col!=".", "Defining a number such as " << col << " as a variable name can lead to serious mistakes.");
	    table[col] = *symbcol;
	    if (col!="#") 
	     {
	       if (all_vars_table.find(col)==all_vars_table.end())
		 {
		    all_vars_table[col]=*symbcol;
		    if (opt.find("loess")!=string::npos) 
		     {
			    symbol* symbder = new symbol(col+"_derivative");
			    all_vars_table[col+"_derivative"]=*symbder;
		     }
		 }
	       else 
		 check_macro(col==variable, "Variable " << col 
		    << " duplicate! Use '#' as blackhole variable name.");
	     }
	    cols.push_back(col);
	    if (valid_test!="")
	     {
		parser reader(table);
		ex validvalue = reader(valid_test);
		ex* valid= new ex((*symbcol)-validvalue);
		if (debug) trace_mac(2,"Testing "<< *valid);
		valid_tests.push_back(valid);
		valid_tests_nature.push_back(label_test(test_nature));
	     }
	 }
        isscol >> col;
     }
    size_t linenum=0;
    size_t recordnum=0;
    symbol linenumber("LINENUMBER");
    symbol recordnumber("RECORDNUMBER");
    table["LINENUMBER"]=linenumber;
    table["RECORDNUMBER"]=recordnumber;
    parser reader(table);
    ex T = reader(variable).evalf();
    ifstream file(filename.c_str());
    if (!file.is_open()) error_macro("Datafile '"<<filename<<"' couldn't be opened.");
    string line;
    for (int i=0; i<skip_lines_in_file && file.good(); i++) 
     { 
       getline(file, line);
       while (line[0]=='#') getline(file, line);
       if (debug) trace_mac(2, "Skipping line "<<i+1<<"/"<<skip_lines_in_file<<": " << line );
     }
    while (file)
     {
        bool all_are_nan=true;
    	record_t rec;
    	exmap map_rec;
	// read the line
	getline(file, line);
	if (!file.good()) 
	 {
     	   if (debug) trace_mac(1, "Completed reading " << filename); 
	   return;
	 }
	if (line.length()==0 ||line[0]=='#') continue;
	istringstream issline(line);
	fieldnames_t::const_iterator icol=cols.begin();
	fieldnames_t::const_iterator ecol=cols.end();
	bool incomplete=false;
	// store values in 'record' and in evaluation table for ginac
	if (separator=="")
	 {
	   string sepname="";
	   if (line.find("	")!=string::npos ||  line.find("	")!=string::npos)  
	   	{ sepname="tab"; separator="	"; }
	   else  if (line.find(" ")!=string::npos) 
	   	{ sepname="space"; separator=" "; }
	   else  if (line.find(";")!=string::npos)  
	   	{ sepname="semicolon"; separator=";";  }
	   else  if (line.find(",")!=string::npos)  
	   	{ sepname="coma"; separator=",";  }
	   else error_macro("Autodetection of separator failed in file " << filename); 
	   if (debug) trace_mac(2,"Autodetected separator '" << separator 
	   	<< "' (" << sepname << ")");
	 }
	else if (separator.length()>1)
	 {
	   if (separator=="tab") separator="	";
	   else if (separator=="space") separator=" ";
	   else if (separator=="semicolon") separator=";";
	   else if (separator=="comma") separator=",";
	   else error_macro("Unknown separator `" << separator << "'");
	 }

       double value;
       size_t pos=line.find(separator), len=0;
       for ( ; icol!=ecol ; icol++) 
	{ 
	    if (pos==0) { value=numeric_limits<double>::max(); }
	    else 
	     {
		    if (debug) {
		      cerr << "In '" << line << "'";
		      cerr << "using " << line.substr(0,pos);
		     }
		    istringstream issa(line.substr(0,pos));
		    if (! (issa >> value)) { incomplete=true; value=numeric_limits<double>::max(); }
			else all_are_nan=false;
		    if (pos==string::npos) line=""; else line=line.substr(pos+1,line.length()-pos);
		    if (debug) { cerr << " read as " << value << " leaving " << line << endl; }
	     }
	    if (*icol!="#")
	     {
		    if (value==numeric_limits<double>::max()) incomplete=true; 
			    else all_are_nan=false;
		    rec.insert(pair<string, double>(*icol, value));
		    map_rec[table[*icol]]=value;
		    if (debug) trace_mac(4, "Insert " << *icol << " -> " << value);
	     }
	    pos=line.find(separator);
	}

	// check that all filters are verified
	bool valid=true;
	auto ve=valid_tests.end();
	auto vt=valid_tests_nature.begin();	
	for (auto vi=valid_tests.begin(); 
		vi!=ve; vi++, vt++)
	 {
	   if (debug) cerr << "Testing " << (**vi) << "==" << (**vi).subs(map_rec).evalf()
	   	<< " gives " << ((**vi).subs(map_rec).evalf().is_zero()?"true":"false") << endl;
	   valid=valid && perform_test(*vt, (**vi).subs(map_rec).evalf(), par.testepsilon);
	   if (debug) cerr << "With test " << *vt << " gives " << (valid?"true":"false") << endl;
	 }

	// calculate current time
	map_rec[table["LINENUMBER"]]=linenum;
	map_rec[table["RECORDNUMBER"]]=recordnum;
	double current_T;
	try { 
	   current_T = ex_to<numeric>(T.subs(map_rec).evalf()).to_double();
	 } catch (exception& p) 
	 { cerr << "Expression `" << T.subs(map_rec).evalf() << "' cannot be evaluated." << endl << p.what() << endl; exit(1); }
	linenum++;
	if (valid) recordnum++;
	if (debug) trace_mac (2, 
		"#" << filename << ": " << variable << T 
		<< "=" << current_T << (is_a<numeric>(T.subs(map_rec))?"v":"f"));
	
	// insert data
	if (! (incomplete && skip_incomplete) 
		&& valid // && !all_are_nan 
		&& (current_T >= par.varmin) 
		&& (current_T <= par.varmax)
		&& (current_T < numeric_limits<double>::max())) data.insert(pair<double, record_t>(current_T, rec));
     }
 }

bool
linear_interpolate_and_insert(double T, const datafile_t& datafile, record_t& current_data, const parameters& par)
 {
    datafile_t::const_iterator iTsup = datafile.lower_bound(T);
    if (iTsup == datafile.end()) 
     { 
       if (debug) trace_mac(4, "index larger than any data stored: not extrapolating");
       return false;  
     }
    if ((*iTsup).first==T) 
     { 
	record_t::const_iterator ic=(*iTsup).second.begin();
	record_t::const_iterator ec=(*iTsup).second.end();
	for ( ; ic!=ec; ic++) current_data.insert(*ic);
	if (debug) trace_mac(4, "data at T");
	return true; 
     }
    if (iTsup == datafile.begin()) 
     { 
       if (debug) trace_mac(4, "data smaller than any data stored: not extrapolating");
       return false;  
     }
    datafile_t::const_iterator iTinf = iTsup; iTinf--;
    double cinf = ((*iTsup).first-T) / ((*iTsup).first - (*iTinf).first);
    double csup = (T-(*iTinf).first) / ((*iTsup).first - (*iTinf).first);
    if (debug) trace_mac(4, "Using " << (*iTinf).first << " " << T << " " << (*iTsup).first << ", coeffs=" << cinf << " and " << csup);
    record_t rec_inf = (*iTinf).second;
    record_t rec_sup = (*iTsup).second;
    record_t::const_iterator irec_inf = rec_inf.begin();
    record_t::const_iterator irec_sup = rec_sup.begin();
    record_t::const_iterator erec_inf = rec_inf.end();
    for ( ; irec_inf!=erec_inf; irec_inf++, irec_sup++)
     {
	if ((*irec_inf).second==numeric_limits<double>::max() || (*irec_sup).second==numeric_limits<double>::max())
 	 {
		if (debug) trace_mac(4, "Don't insert " << (*irec_inf).first << " interp " << (*irec_inf).second << " " << (*irec_sup).second );
		continue;
	 }
     	current_data.insert(
	  pair<string,double>((*irec_inf).first, cinf*(*irec_inf).second + csup*(*irec_sup).second));
	if (debug) trace_mac(4, "Insert " << (*irec_inf).first << " interp " << (*irec_inf).second << " " << (*irec_sup).second );
     }
    return true;
 }


bool
find_previous_and_insert(double T, const datafile_t& datafile, record_t& current_data, const parameters& par)
 {
        #if __cplusplus>=201103L 
        if (debug) trace_mac(2,"Interpolation with {previous} value guaranteed to work thanks to C++-11 compliance of compiler");
        #elif __cplusplus==201103L
        if (!debug) 
		error_macro("DataMerge was compiled with C++ " << __cplusplus << " which does not guarantee\n"
			<<  "interpolation with {previous} value to work. Use `--debug` flag to force usage\n"
			<<  "or recompile with uptodate compiler.");
	else trace_mac(2,"******* Forced interpolation with {previous} with non-C++-11 compliant compiler. Results may be false ***");
        #else
        std::cout << "C++" << std::endl;
        #endif
    datafile_t::const_iterator iTsup = datafile.upper_bound(T);
    if (iTsup == datafile.end()) 
     { 
       if (debug) trace_mac(4, "index larger than any data stored -- still valid");
     }
    if ((*iTsup).first==T)  // shouldn't happen with upper_bound
     { 
	record_t::const_iterator ic=(*iTsup).second.begin();
	record_t::const_iterator ec=(*iTsup).second.end();
	for ( ; ic!=ec; ic++) current_data.insert(*ic);
	if (debug) trace_mac(4, "data at T");
	return true; 
     }
    if (iTsup == datafile.begin()) 
     { 
       if (debug) trace_mac(4, "data smaller than any data stored: not extrapolating");
       return false;  
     }
    datafile_t::const_iterator iTinf = iTsup; iTinf--;
    record_t rec_inf = (*iTinf).second;
    record_t::const_iterator irec_inf = rec_inf.begin();
    record_t::const_iterator erec_inf = rec_inf.end();
    for ( ; irec_inf!=erec_inf; irec_inf++)
     {
	if ((*irec_inf).second==numeric_limits<double>::max())
 	 {
		if (debug) trace_mac(4, "Don't insert " << (*irec_inf).first << " interp " << (*irec_inf).second  );
		continue;
	 }
     	current_data.insert(
	  pair<string,double>((*irec_inf).first, (*irec_inf).second));
     }
    return true;
 }

double
loess_weight(double T0, double T1, double dT)
{
  return 1 - (T0-T1)*(T0-T1)*(T0-T1)/(dT)/(dT)/(dT);
}

bool
loess0_interpolate_and_insert(double T, const datafile_t& datafile, record_t& current_data, const parameters& par)
 {
    check_macro(par.Tsmooth!=0, "LOESS interpolation called, but least squares bandwidth unset! Use --bandwidth.");
    warning_macro("Using loess0 and I don't know what it is anymore");
    record_t rec, rec_weight;
    datafile_t::const_iterator iT = datafile.lower_bound(T - par.Tsmooth);
    if (iT == datafile.end()) 
     { 
       if (debug) trace_mac(4, "larger than any data stored");
       return false;  
     }
    record_t::const_iterator ic=(*iT).second.begin();
    record_t::const_iterator ec=(*iT).second.end();
    double weight = loess_weight(T, (*iT).first, par.Tsmooth);
    for ( ; ic!=ec; ic++) 
     { 
       double value = (*ic).second;
       if (value != numeric_limits<double>::max())
        {
	  rec_weight.insert(pair<string,double>((*ic).first,weight));
       	  rec.insert(pair<string,double>((*ic).first,weight*value));
	}
     }
    iT++;
    while ((*iT).first < T + par.Tsmooth)
     {
	record_t::const_iterator ic=(*iT).second.begin();
	record_t::const_iterator ec=(*iT).second.end();
	weight = loess_weight(T, (*iT).first, par.Tsmooth);
	for ( ; ic!=ec; ic++) 
	 { 
	  double value = (*ic).second;
	  if (value != numeric_limits<double>::max())
	   {
	  	rec_weight[(*ic).first]+=weight;
		rec[(*ic).first]+=weight*value;
	   }
	 }
	iT++;
     }
    record_t::const_iterator irec = rec.begin();
    record_t::const_iterator irec_weight = rec_weight.begin();
    record_t::const_iterator erec = rec.end();
    for ( ; irec!=erec; irec++, irec_weight++)
     {
        if ((*irec_weight).second!=0)
     		current_data.insert(
	  	  pair<string,double>((*irec).first, (*irec).second/(*irec_weight).second));
	else current_data.insert(pair<string,double>((*irec).first, numeric_limits<double>::max()));
	if (debug) trace_mac(4, "Insert " << (*irec).first << " interp " << (*irec).second << " " << (*irec_weight).second );
     }
    return true;
 }

bool
loess1_interpolate_and_insert(double T, const datafile_t& datafile, record_t& current_data, const parameters& par)
 {
    check_macro(par.Tsmooth!=0, "LOESS interpolation called, but least squares bandwidth unset! Use --bandwidth.");
    record_t rec, rec_weight;
    datafile_t::const_iterator iT = datafile.lower_bound(T - par.Tsmooth);
    // experimental: exit if before start of data
    if (iT==datafile.begin()) 
     {
       if (debug) trace_mac(4, "not extrapolating for t=" << T << " < T_min - " << par.Tsmooth);
       return false;
     }
    if (datafile.upper_bound(T + par.Tsmooth) == datafile.end())
     {
       if (debug) trace_mac(4, "not extrapolating for t=" << T << " > T_max + " << par.Tsmooth);
       return false;
     }
    datafile_t::const_iterator eT = datafile.end();
    if (iT == eT) 
     { 
       if (debug) trace_mac(4, "larger than any data stored");
       return false;  
     }
    // count the number of points
    datafile_t::const_iterator iT1 = iT;
   size_t size=0;
    while ((*iT1).first < T + par.Tsmooth) // && iT1!=eT)
     { 
     	size++; iT1++; 
	if (iT1==eT) 
	 { 
	   size--; 
	   if (debug) trace_mac(4, "not extrapolating for t=" << T << " > T_max + " << par.Tsmooth);
	   return false;
	 }
     }
    // experimental: exit if reaching end of data
    if (iT1==eT) 
     {
       iT1--;
       if (debug) trace_mac(4, "not extrapolating for t=" << T << " after "  << size << " points recorded"
       	<< endl << "Start was " << (*iT).first << " current is " << (*iT1).first << " smooth " << par.Tsmooth);
       return false;
     }

    iT1=iT;

    double* x = new double[size];
    vector<double*> y((*iT).second.size()); 
    vector<double*> w((*iT).second.size());
    vector<size_t> n((*iT).second.size(),0);
    for (size_t i = 0; i < (*iT).second.size() ; i ++ )
    	{ y[i] = new double[size]; w[i] = new double[size]; }
    double* wmax = new double[(*iT).second.size()];

    record_t::const_iterator ic=(*iT).second.begin();
    record_t::const_iterator ec=(*iT).second.end();
    double weight = loess_weight(T, (*iT).first, par.Tsmooth);
    for (size_t i=0 ; ic!=ec; i++, ic++) 
     { 
       double value = (*ic).second;
       w[i][0]=weight;
       x[0]=(*iT).first;
       y[i][0]=value;
       if (value == numeric_limits<double>::max()) w[i][0] = 0;
       else n[i]++;
       if (debug) trace_mac(5, "adding " << y[i][0] << " " << w[i][0] << " " << n[i]);
     }
    iT++;
    size_t j=1;
    while ((*iT).first < T + par.Tsmooth && iT != eT)
     {
	record_t::const_iterator ic=(*iT).second.begin();
	record_t::const_iterator ec=(*iT).second.end();
	weight = loess_weight(T, (*iT).first, par.Tsmooth);
	for ( size_t i=0 ; ic!=ec; i++, ic++) 
	 { 
	       double value = (*ic).second;
	       w[i][n[i]]=weight;
	       x[n[i]]=(*iT).first;
	       y[i][n[i]]=value;
       	       if (debug) trace_mac(5, "adding " << y[i][n[i]] << " " << w[i][n[i]] << " " << n[i]);
	       if (value == numeric_limits<double>::max()) w[i][n[i]] = 0;
	       else n[i]++;
	 }
	iT++; j++; 
     }
    record_t::const_iterator irec = (*iT1).second.begin();
    record_t::const_iterator erec = (*iT1).second.end();
    for ( size_t i=0 ; irec!=erec; irec++, i++)
     {
        if (n[i]>=2)
	 {
		double c0, c1, cov00, cov01, cov11, chisq;
	 	gsl_fit_wlinear(x, 1, w[i], 1, y[i], 1, n[i], &c0, &c1, &cov00, &cov01, &cov11, &chisq);
		if (debug) trace_mac(4, "Insert " << (*irec).first << " interp " << c0+c1*T << "=" << c0 << "+T*" << c1 );
		if (isfinite(c0)&&isfinite(c1))
		 {
     			current_data.insert(pair<string,double>((*irec).first, c0 + c1*T));
     			current_data.insert(pair<string,double>((*irec).first+"_derivative", c1));
		 }
		else
		 {
			current_data.insert(pair<string,double>((*irec).first, numeric_limits<double>::max()));
			current_data.insert(pair<string,double>((*irec).first+"_derivative", numeric_limits<double>::max()));
		 }
	 }
	else if (n[i]==1 && x[0]==T)
	 {
	 	if (debug) trace_mac(1, "Inserting exact value rather than interpolation at " << T 
			<< " since there is no other point in window");
     		current_data.insert(pair<string,double>((*irec).first, y[i][0]));
		current_data.insert(pair<string,double>((*irec).first+"_derivative", numeric_limits<double>::max()));
	 }
	else 
	 {
	 	if (debug) trace_mac(1, "Not inserting interpolated points at " << T << " since only " << n[i] 
			<< " records in time window of size " << par.Tsmooth);
	 	current_data.insert(pair<string,double>((*irec).first, numeric_limits<double>::max()));
	 	current_data.insert(pair<string,double>((*irec).first+"_derivative", numeric_limits<double>::max()));
	 }
     }
    return true;
 }

bool
interpolate_and_insert(double T, const datafile_t& datafile, record_t& current_data, const string& opt, const parameters& par)
 {
    if (opt.find("loess0")!=string::npos)
    	return loess0_interpolate_and_insert(T, datafile, current_data, par);
    else if (opt.find("loess")!=string::npos)
    	return loess1_interpolate_and_insert(T, datafile, current_data, par);
    else if (opt.find("previous")!=string::npos)
    	return find_previous_and_insert(T, datafile, current_data, par);
    else // default -- if (opt.find("linear")!=string::npos) 
    	return linear_interpolate_and_insert(T, datafile, current_data, par);
 }

fieldnames_t 
expand_multicol (string c)
 {
    list<string> trange, output;
    list<int> tstart;
    list<int> tend;
    list<int> tstep;
    size_t n=0;
    size_t idx=c.find("[");
    while(idx!=string::npos) 
     {
	string multicol_base = c.substr(0,idx);
	size_t idx2=c.find("]",idx);
	check_macro(idx2!=string::npos,
		"Multicolumn variable syntax: " << multicol_base 
		<< "[<number_of_columns>|<first_index>:<last_index>|<first_index>:<last_index>:<step>]");
	trange.push_back(c.substr(idx+1,idx2-idx-1));
	size_t idxdot=c.find(":",idx);
	int first_index=0;
	int last_index=0;
	try {
	if (idxdot==string::npos||idxdot>idx2) 
	 {
	   istringstream issn(c.substr(idx+1,idx2-idx-1)); issn >> last_index; last_index--; // because for n it's [0,..,n-1] 
	   if (n==0) { n=last_index+1; }
	   else { check_macro(n==last_index+1,
	    "Ranges " << *(trange.begin()) << " and " << *(trange.rbegin()) << " are incompatible in --output statement"); } 
	   tstart.push_back(0);
	   tend.push_back(last_index);
	   tstep.push_back(1);
	 }
	else
	 {
	   size_t idxdot2=c.find(":",idxdot+1);
	   int step;
	   if (idxdot2!=string::npos && idxdot2<idx2) 
	    {  
	      istringstream isss(c.substr(idxdot2+1,idx2-idxdot2-1)); isss >> step;
	      idx2=idxdot2;
	    }
	   else step=1;
	   istringstream issf(c.substr(idx+1,idxdot-idx-1)); issf >> first_index;
	   istringstream issl(c.substr(idxdot+1,idx2-idxdot-1)); issl >> last_index;
	   check_macro((last_index-first_index)/step+1>0,"Empty range " << *(trange.rbegin()) << " in --output statement"); 
	   if (n==0) { n=(last_index-first_index)/step+1; }
	   else { check_macro(n==(last_index-first_index)/step+1,
	    "Ranges " << *(trange.begin()) << " and " << *(trange.rbegin()) << " are incompatible in --output statement"); } 
	   tstart.push_back(first_index);
	   tend.push_back(last_index);
	   tstep.push_back(step);
	 }
	}  catch (exception& p) { error_macro(
		"Multicolumn variable syntax: " << multicol_base 
		<< "[<number_of_columns>|<first_index>..<last_index>]"); }
       idx=c.find("[",idx+1);
     }
    for (size_t counter=0 ; counter<n ; counter++ )
     {
       string d=c;
       auto irange = trange.begin();
       auto erange = trange.end();
       auto istart = tstart.begin();
       auto istep = tstep.begin();
       for ( ; irange!=erange ; irange++, istart++, istep++)
	{
	  regex range("\\["+(*irange)+"\\]");
	  d=regex_replace(d,range,"__"+to_string(*istart+*istep*counter));
	}
       output.push_back(d);
     }
   return output;
 }


void cmdline_error(string p, string opt="")
{
  if (opt!="") cerr << "Unknown option or unsufficient number of arguments: " << opt << endl << endl;
  cerr << "Usage: "<<p<<" --reference|-r <filename> '<time expression>' 'var_r1 var_r2 var_r3...'" << endl;
  cerr << "\t [--input|-i <filename> '<time expression>[{<interpolation>}]' 'var_i1[==filter_i1] var_i2[>=filter_i2] var_i3[<filter_i3] ...' [-i ...] ...]" << endl;
  cerr << "\t [--constant <variable_name> <value>]" << endl;
  cerr << "\t --output|-o <filename> '<time var name>' '<var_o1[+=expr]> <var_o2[*=expr]> <var_o3>...'" << endl;
  cerr << "\t --separator|-s <character> --skip_header <number_of_lines> --output_header_commented" << endl;
  cerr << "\t --filter_epsilon <value=0> [--cumulate_through]" << endl;
  cerr << endl << "<interpolation> is either 'linear', 'previous' or 'loess'" << endl;
  cerr << "\t <time expression> are algebraic expressions involving any of the input var" << endl;
  cerr << "\t   of the current file, and possibly LINENUMBER keyword (starts 0 for 1st row)" << endl;
  cerr << "\t   or RECORDNUMBER keyword (numbering filtered records)" << endl;
  cerr << "\t <var_o?> are algebraic expressions involving any of the reference and input var" << endl;
  cerr << "\t   and may additionally make use of common math functions and helper functions" << endl;
  cerr << "\t   such as min, max, mean, median, stat_n with syntax as 'min4(v1,v2,v3,v4)'" << endl;
  cerr << "\t For a complete documentation, see `README.md` file." << endl;
  cerr << endl << "Copyright (C) 2012-2022 Jocelyn Etienne, LIPHY Grenoble, CNRS" << endl; 
  exit(1);
}

int main (int argc, char**argv)
{
  dataset_t all_data;
  datafile_t ref_data;
  auto emop = multi_operators.end();

  parameters par;
  bool reference_set=false, output_set=false, force_write=false, skip_missing=false, 
  	stop_cumulating_after_NA=true, ignore_NA_in_multicol_op=false;
  size_t skip_lines=0;
  string reference_file, reference_file_variable, reference_file_columns;
  string output_file, variable;
  string separator="";
  string output_separator="	";
  string header_leader="";
  fieldnames_t files, output, cumulator, variables, file_options, columns;
  cumulator_t cumul_mult, cumul_add;
  list<pair<string,fieldnames_t> > multi_op;
  symtab all_vars_table;
  // read command line
  for (int i=1; i<argc; i++)
   {
	string s=argv[i];
	// Flags
	if (s=="--debug"        ||s=="-d")      { debug=true; continue;  }
	if (s=="--version")      { cerr << "DataMerge (C) Jocelyn Etienne, version " << VERSION << endl; exit(1);  }
	if (s=="--dump_multicol_operators")	
		{ copy(multi_operators.begin(), multi_operators.end(), ostream_iterator<string>(cout, " ")); exit(0); }
	if (s=="--help")         { cmdline_error(argv[0]); exit(0);  }
	if (s=="--force"        ||s=="-f")      { force_write=true; continue;  }
	if (s=="--skip_missing" ||s=="-S")      { skip_missing=true; continue;  }
	if (s=="--output_header_commented")	{ header_leader="#"; continue; }
	if (s=="--cumulate_through")		{ stop_cumulating_after_NA=false; ignore_NA_in_multicol_op=true; continue; }
	// 1-arg
	i++;
	if (i==argc) cmdline_error(argv[0],s+" requires 1 argument");
        string a=argv[i];
	if (s=="--bandwidth")			{ istringstream issa(a); issa>>par.Tsmooth; par.Tsmooth/=2; continue; }
	if (s=="--filter_epsilon")		{ istringstream issa(a); issa>>par.testepsilon;; continue; }
	if (s=="--separator"	||s=="-s")	{ separator=a; continue; }
	if (s=="--range_min") 			{ istringstream issa(a); issa>>par.varmin; continue; }
	if (s=="--range_max") 			{ istringstream issa(a); issa>>par.varmax; continue; }
	if (s=="--skip_header")			{ istringstream issa(a); issa>>skip_lines; continue; }
	// 2-args
	i++;
	if (i==argc) cmdline_error(argv[0],s+" requires 2 arguments");
        string b=argv[i];
	if (s=="--constant")	
	 { 
	   istringstream issb(b); 
	   double value;
	   issb>>value; 
	   all_vars_table[a]=value;
	   continue; 
	 }
	// 3-args
	i++;
	if (i==argc) cmdline_error(argv[0],s+" requires 3 arguments");
        string c=argv[i];
        if (s=="--reference"	||s=="-r") 
	  {	
	  	check_macro(!reference_set, "Only one reference file should be passed");
		reference_set=true;
	  	reference_file=a;
		reference_file_variable=b;
		reference_file_columns=c;
	  	continue; 
	  }
        if (s=="--input"	||s=="-i") 
	  {	
	  	files.push_back(a);
		size_t pos1 = b.find("{");
		if (pos1!=string::npos)
		 {
		   size_t pos2 = b.find("}");
		   if (pos2!=string::npos && pos2>pos1+1)
		    {
		      string opt=b.substr(pos1+1, pos2-pos1-1);
		      file_options.push_back(opt);
		      b=b.substr(0, pos1);
		    }
		   else file_options.push_back("");
		 }
		else file_options.push_back("");
		variables.push_back(b);
		columns.push_back(c);
	  	continue; 
	  }
        if (s=="--output"	||s=="-o") 
	  {	
	  	check_macro(!output_set, "Only one output file should be passed");
		output_set=true;
		if (a!="-")
		    output_file=a;
		else
		 {
		    output_file="/dev/stdout";
		    force_write=true;
		 }
		variable=b;
		istringstream issc(c); issc >> c; 
		while(issc) 
		 {  
		    size_t idx=c.find("'") ;
		    while ( idx !=string::npos)
		     { 	
		     	c.replace(idx, 1, "_derivative");
		     	idx=c.find("'"); 
		     }
		    auto imop = multi_operators.begin();
		    for ( ; imop!=emop ; imop++)
		     {
		      idx=c.find(*imop);
		      if (idx!=string::npos)
		       {
		        check_macro(idx+(*imop).size()<c.size() && (c[idx+(*imop).size()] == '{'),
				"Error in `" << c << "', syntax is `" << *imop << "{expr}'" ); 
			size_t idx2 = c.find("}",idx+(*imop).size());
			check_macro(idx2!=string::npos && !(idx2>c.find("{",idx+(*imop).size()+1)),
				"Error in `" << c << "', syntax is `" << *imop << "{expr}', no nested `{}' allowed."); 
			string operand=c.substr(idx+(*imop).size()+1,idx2-idx-(*imop).size()-1);
			stringstream issi; issi << multi_op.size();
			string result=(*imop)+"__"+issi.str(); 
			c.replace(idx,idx2-idx+1,result); // rename column to the variable name of result
			fieldnames_t alloperands = expand_multicol(operand);
			auto op=pair<string,fieldnames_t>(result, alloperands);
			multi_op.push_back(op);
		       }
		     }
		    if (c.find("[")!=string::npos)
		     {
			fieldnames_t multicol = expand_multicol(c);
			auto ifield = multicol.begin();
			auto efield = multicol.end();
			for ( ; ifield!=efield ; ifield++ ) 
			   output.push_back(*ifield);
		     }
		    else
		     {
		       idx=c.find("=");
		       if (idx!=string::npos) {
			   	if (c.find("*")==idx-1)
				 	cumul_mult.push_back(pair<size_t,string>(output.size(),c.substr(idx+1)));
				else if (c.find("+")==idx-1) 
				 	cumul_add.push_back(pair<size_t,string>(output.size(),c.substr(idx+1)));
				else error_macro("Cumulator construct not understood `" << c << "`, syntax is `S+=expression` or `P*=expression`");
				c=c.substr(0,idx-1);
				cumulator.push_back(c);
			    }
		     output.push_back(c);
		     }
		    issc >> c; 
		 }
	  	continue; 
	  }
	cmdline_error(argv[0], s);
   }
  if (! (reference_set)) cmdline_error(argv[0], "--reference missing");
  if (! (output_set)) cmdline_error(argv[0], "--output missing");
  if (file_exists(output_file) && !force_write) error_macro("File " << output_file << " already exists, use -f to force overwriting.");

  // read the data
  read_data(reference_file, reference_file_variable, reference_file_columns, "",
  	ref_data, all_vars_table, par, separator, skip_lines);
  fieldnames_t::const_iterator ifile = files.begin();
  fieldnames_t::const_iterator ifile_opt = file_options.begin();
  fieldnames_t::const_iterator ivar = variables.begin();
  fieldnames_t::const_iterator icol = columns.begin();
  fieldnames_t::const_iterator efile = files.end();
  for ( ; ifile != efile ; ifile++, ivar++, icol++, ifile_opt++ )
   {
    datafile_t new_data;
    read_data(*ifile, *ivar, *icol, *ifile_opt, new_data, all_vars_table, par, separator, skip_lines);
    all_data.push_back(new_data);
   }

  // write header	
  ofstream fout(output_file.c_str(), ios::trunc);
  fout << header_leader << variable;
  fieldnames_t::const_iterator ifield = output.begin();
  fieldnames_t::const_iterator efield = output.end();
  cumulator_t loc_cumul_add = cumul_add;
  cumulator_t loc_cumul_mult = cumul_mult;
  for ( size_t outpos=0 ; ifield != efield ; ifield++, outpos++)
   {
	string field=*ifield;
	string der="_derivative";
	size_t pos=field.find(der);
	if (pos!=string::npos) field.replace(pos, der.length(), "'");
	if (should_cumul(loc_cumul_add,outpos))
	       {
		 field+="__+"; // if it is a cumulator column, value is incremented by line's operation, whereas same variable refers to previous line result elsewhere
		 loc_cumul_add.pop_front();
	       }
	if (should_cumul(loc_cumul_mult,outpos))
	       {
		 field+="__+"; // if it is a cumulator column, value is incremented by line's operation, whereas same variable refers to previous line result elsewhere
		 loc_cumul_mult.pop_front();
	       }
  	fout << output_separator << field;
   }
  fout << endl;

  // prepare time and cumulator variables
  symbol* symbcol = new symbol(variable);
  check_macro(all_vars_table.find(variable)==all_vars_table.end(), 
  	"Time variable name `" << variable << "' already used as a column or constant name");
  all_vars_table[variable]=*symbcol;
  exmap map_cumul;
  auto icum_add = cumul_add.begin();
  auto ecum_add = cumul_add.end();
  auto icum_mult = cumul_mult.begin();
  auto ecum_mult = cumul_mult.end();
  auto iout = output.begin(); size_t last_add=0;
  for ( ; icum_add!=ecum_add; icum_add++) 
   {
      advance(iout, (*icum_add).first-last_add);
      last_add=(*icum_add).first;
      check_macro(all_vars_table.find(*iout)==all_vars_table.end(), 
	    "Cumulator variable name `" << *iout << "' already used as a column or constant name");
      symbol* symbcol = new symbol(*iout);
      all_vars_table[*iout]=*symbcol;
      map_cumul[all_vars_table[*iout]]=0; // initialize to 0 for additions
      if (debug) cerr << *iout << " set 0" << endl;
   }
  iout = output.begin(); last_add=0;
  for ( ; icum_mult!=ecum_mult; icum_mult++) 
   {
      advance(iout, (*icum_mult).first-last_add);
      last_add=(*icum_mult).first;
      check_macro(all_vars_table.find(*iout)==all_vars_table.end(), 
	    "Cumulator variable name `" << *iout << "' already used as a column or constant name");
      symbol* symbcol = new symbol(*iout);
      all_vars_table[*iout]=*symbcol;
      map_cumul[all_vars_table[*iout]]=1; // initialize to 1 for products
      if (debug) cerr << *iout << " set 1" << endl;
   }
  auto eop=multi_op.end();
  for ( auto iop=multi_op.begin(); iop!=eop ; iop++ )
   {
	symbol* symbop = new symbol((*iop).first);
      	all_vars_table[(*iop).first]=*symbop;
   }

  // write data
  datafile_t::const_iterator iref = ref_data.begin();
  datafile_t::const_iterator eref = ref_data.end();
  record_t current_data;
  size_t lines_written=0;
  if (iref==eref) cerr << "No records found, or all were incomplete. Use '#' column title to ignore a column. Missing data management not implemented yet.";
  for ( ; iref!=eref ; iref++ )
   {
	if ((*iref).first==numeric_limits<double>::max()) continue;
   	if (debug) trace_mac(1, "Writing " << (*iref).first);
   	// Calculate this data in all files
	dataset_t::const_iterator ifile = all_data.begin();
	dataset_t::const_iterator efile = all_data.end();
	fieldnames_t::const_iterator iopt = file_options.begin();
	fieldnames_t::const_iterator eopt = file_options.end();
	current_data = (*iref).second; // Data at current position in the reference file

	for ( ; ifile != efile ; ifile++, iopt++ ) // Scan the other files
	  interpolate_and_insert((*iref).first, *ifile, current_data, *iopt, par);

	if (debug)
	 {
		record_t::const_iterator ic=current_data.begin();
		record_t::const_iterator ec=current_data.end();
		for ( ; ic!=ec; ic++) cerr << (*ic).first << "=" << (*ic).second << " ";
	 }

	// prepare Ginac variables, and create the time variable
	parser reader(all_vars_table);
	exmap map_rec;

	// insert current time as a variable
	map_rec[all_vars_table[variable]] = (*iref).first ;

	record_t::const_iterator ic=current_data.begin();
	record_t::const_iterator ec=current_data.end();
	if (debug) cerr << "Data available :" ;
	for ( ; ic!=ec; ic++) {
	  check_macro((*ic).first!=variable, "Found " << variable << " in data with val " << (*ic).second << " and not " << (*iref).first);
	  if ((*ic).second == numeric_limits<double>::max())
	  	map_rec[all_vars_table[(*ic).first]] = NA;
	  else 
	   { 
	  	map_rec[all_vars_table[(*ic).first]] = (*ic).second;
	  	if (debug) cerr << (*ic).first << "=" << (*ic).second << ","; 
	   }
	  }
	
	auto icum=cumulator.begin();
	auto ecum=cumulator.end();
	for ( ; icum!=ecum ; icum++) 
	  {
	   auto val = map_cumul[all_vars_table[*icum]];
	   if (val == numeric_limits<double>::max())
	        map_rec[all_vars_table[*icum]] = NA;
	   else map_rec[all_vars_table[*icum]] = val;
	   if (debug) cerr << "Cumul var " << *icum << " is " << map_cumul[all_vars_table[*icum]] << endl;
	  }

	// start with multi-column operators
	auto iop=multi_op.begin();
	auto eop=multi_op.end();
	for (size_t oppos=0 ; iop != eop ; iop++, oppos++)
	 {
	   auto ioperand = (*iop).second.begin();
	   auto eoperand = (*iop).second.end();
	   list<double> vals;
	   bool has_NA=false;
	   for ( ; ioperand!=eoperand ; ioperand++ )
	     {
	      ex u;
	      try {
		  u = (reader((*ioperand))).subs(map_rec).evalf();
	      }  catch (exception& p) { error_macro("Expression `" << (*ioperand) << "' cannot be evaluated." << endl << p.what()) }
	      double val;
	      if (is_a<numeric>(u))
	       {
		 try { 
		   val =  ex_to<numeric>(u).to_double(); 
		 } catch (exception& p) { error_macro("Expression `" << u << "' cannot be evaluated." << endl << p.what()) }
		 if (val == numeric_limits<double>::max())
		  { if (!ignore_NA_in_multicol_op) { has_NA=true; vals.push_back(numeric_limits<double>::max()); } }
		 else vals.push_back(val);
	       }
	      else if (!ignore_NA_in_multicol_op) { has_NA=true; vals.push_back(numeric_limits<double>::max()); }
	     }
	    if (!has_NA || ignore_NA_in_multicol_op)
		map_rec[all_vars_table[(*iop).first]] = multi_op_manage((*iop).first, vals);
	    else map_rec[all_vars_table[(*iop).first]] = NA;
	 }
	// now output the columns
	fout << (*iref).first ;
	fieldnames_t::const_iterator ifield = output.begin();
	double value;
	cumulator_t loc_cumul_add = cumul_add;
	cumulator_t loc_cumul_mult = cumul_mult;
	for (size_t outpos=0 ; ifield != efield ; ifield++, outpos++)
	 {
	  /*
	  record_t::const_iterator ic=current_data.find(*ifield);
	  if (ic!=ec) value =(*(current_data.find(*ifield))).second;
	  else value = numeric_limits<double>::max();
	  if (value == numeric_limits<double>::max()) fout << output_separator << notavailable;
	  else fout << output_separator << value;
	  */
	  ex u;
	  if (should_cumul(loc_cumul_add,outpos)) 
	   {
	      try {
	      u = (reader((*loc_cumul_add.begin()).second)).subs(map_rec).evalf();
	      }  catch (exception& p) { error_macro("Expression `" << (*loc_cumul_add.begin()).second << "' cannot be evaluated." << endl << p.what()) }
	   }
	  else if (should_cumul(loc_cumul_mult,outpos)) 
	   {
	      try {
	      u = (reader((*loc_cumul_mult.begin()).second)).subs(map_rec).evalf();
	      }  catch (exception& p) { error_macro("Expression `" << (*loc_cumul_mult.begin()).second << "' cannot be evaluated." << endl << p.what()) }
	   }
	  else
	   {
	      try {
	      u = (reader(*ifield)).subs(map_rec).evalf();
	      }  catch (exception& p) { error_macro("Expression `" << *ifield << "' cannot be evaluated." << endl << p.what()) }
	   }
	  double val;
	  try { 
	  if (is_a<numeric>(u)) val =  ex_to<numeric>(u).to_double(); 
	  } catch (exception& p) { error_macro("Expression `" << u << "' cannot be evaluated." << endl << p.what()) }
	  if (should_cumul(loc_cumul_add,outpos)) 
	   {
	     if (is_a<numeric>(u)) 
	      {
		 double curr = ex_to<numeric>(map_cumul[all_vars_table[*ifield]]).to_double();
		 if (curr == numeric_limits<double>::max()) fout << output_separator << NA;
		 else
		  {
		     curr += val;
		     map_cumul[all_vars_table[*ifield]] = curr;
		     loc_cumul_add.pop_front();
		     fout << output_separator <<  curr;
		   }	  
	      }
	     else
	      {
	      	if (stop_cumulating_after_NA) map_cumul[all_vars_table[*ifield]] = numeric_limits<double>::max();
	  	fout << output_separator << notavailable;
	      }
	   }
	  else if (should_cumul(loc_cumul_mult,outpos)) 
	   {
	     if (is_a<numeric>(u)) 
	      {
		 double curr = ex_to<numeric>(map_cumul[all_vars_table[*ifield]]).to_double();
		 if (curr == numeric_limits<double>::max()) fout << output_separator << NA;
		 else
		  {
		     curr *= val;
		     map_cumul[all_vars_table[*ifield]] = curr;
		     loc_cumul_mult.pop_front();
		     fout << output_separator <<  curr;
		   }	  
	      }
	     else
	      {
	      	if (stop_cumulating_after_NA) map_cumul[all_vars_table[*ifield]] = numeric_limits<double>::max();
	  	fout << output_separator << notavailable;
	      }
	   }
	  else if (is_a<numeric>(u)) 
	   {
	      fout << output_separator <<  val;
	   }
	  else fout << output_separator << notavailable;
	 }
  	fout << endl;
	lines_written++;
   }
  fout << "# data collated by DataMerge using the command:" << endl;
  fout << "#";
  for (int i=0; i<argc; i++) 
    if (   (string(argv[i]).find("(",0)!=string::npos)
    	|| (string(argv[i]).find("*",0)!=string::npos) 
    	|| (string(argv[i]).find(" ",0)!=string::npos) )
	fout << "'" << argv[i] << "' ";
    else fout << argv[i] << " ";
  fout << endl;
  cerr << "Written " << lines_written << " records to " << output_file << endl;
}
