# DataMerge
 <img align="right" width="125" height="125" src="junk/datamergelogo.png">
 DataMerge is a linux command line which allows to merge a number of datafiles
 organised in columns indexed in a one-dimensional way into a single dataset.
 It can handle files of data tabulated on different grids, by using
 interpolation methods. E.g., data recorded every 10 seconds and data recorded
 every 15 seconds can be interpolated into either grid. Interpolation can be
 linear between adjacent points, or use a least-square approximation on a
 moving window. It also allows to process columns individually, perform
 cross-column calculations or change the unit in which quantities are
 expressed. Several other methods also allow to perform cross-row calculations,
 such as finite differences or normalisation by an automatically detected 
 entry row.

## (C) J Etienne 2012-2022
 DataMerge is released under [GPL 2 licence](LICENSE).
 DataMerge logo CC-BY 4.0 based on original work by [Delapouite](https://delapouite.com/)





## Generate initial datasets

 Some `bash` first to generate a time grid (files also provided in `git`)

     NMAX=40
     RATE=0.01
     seq 0 3 ${NMAX} | tr " " "\n" > example/sampling_3.tsv
     seq 1 7 ${NMAX} | tr " " "\n" > example/sampling_7.tsv

> Sample from output file [`example/sampling_7.tsv`](example/sampling_7.tsv):
>
>     1  
>     8  
>     15  
>     22  
>     29  
>     36  

 Then use `datamerge` to generate data from those time grids. 

     datamerge --force --separator tab \
     	--reference example/sampling_3.tsv 'LINENUMBER+1' time \
     	--constant rate $RATE  \
     	--constant pi '3.14159265' \
     	--output example/dataset_sampling_3.tsv 'line' 'time sin(2*pi*time*rate) exp(time*rate)'

> Sample from output file [`example/dataset_sampling_3.tsv`](example/dataset_sampling_3.tsv):
>
>     line	time	sin(2*pi*time*rate)	exp(time*rate)  
>     1	0	0	1  
>     2	3	0.187381	1.03045  
>     3	6	0.368125	1.06184  
>     4	9	0.535827	1.09417  
>     5	12	0.684547	1.1275  
>     6	15	0.809017	1.16183  
>     7	18	0.904827	1.19722  

 Complex mathematical expressions can be evaluated at output time, using any of the variables
 that have been defined before when reading the input files. The evaluation of these expressions
 is delegated to the [GiNaC](https://www.ginac.de/) library.

 Note that constants can be declared for further use.
 `--force` option overwrites existing output file.
 `--separator` is needed when it cannot be guessed from input file.
 Several option names can be abbreviated.

 We now generate another datafile with a different time grid:

     datamerge -f -s tab \
     	-r example/sampling_7.tsv 'LINENUMBER+1' time \
     	--constant rate $RATE  \
     	--constant pi '3.14159265' \
     	-o example/dataset_sampling_7.tsv 'line' 'time cos(2*pi*time*rate) exp(time*rate)'

> Sample from output file [`example/dataset_sampling_7.tsv`](example/dataset_sampling_7.tsv):
>
>     line	time	cos(2*pi*time*rate)	exp(time*rate)  
>     1	1	0.998027	1.01005  
>     2	8	0.876307	1.08329  
>     3	15	0.587785	1.16183  
>     4	22	0.187381	1.24608  
>     5	29	-0.24869	1.33643  
>     6	36	-0.637424	1.43333  

## Interpolate between datasets
 The time grid for output is the one of the file called by the `--reference` option.
 As many additionnal `--input` files as desired can be used, linear interpolation is performed.
 `na` is inserted if values cannot be interpolated.
 Field names have to be unique.

     datamerge -f \
     	--reference example/dataset_sampling_3.tsv 'time_sampling_3' \
     		'line_sampling_3 time_sampling_3 sinusoidal exponential' \
     	--input example/dataset_sampling_7.tsv 'time_sampling_7' \
     		'line_sampling_7 time_sampling_7 cosinusoidal exponential_sampling_7'	\
     	-o example/linear_interpolation.tsv 'time' \
     		'sinusoidal cosinusoidal exponential exponential-exponential_sampling_7 sinusoidal^2+cosinusoidal^2'

> Sample from output file [`example/linear_interpolation.tsv`](example/linear_interpolation.tsv):
>
>     time	sinusoidal	cosinusoidal	exponential	exponential-exponential_sampling_7	sinusoidal^2+cosinusoidal^2  
>     0	0	na	1	na	na  
>     3	0.187381	0.96325	1.03045	-0.000525714	0.962962  
>     6	0.368125	0.911084	1.06184	-0.000524286	0.96559  
>     9	0.535827	0.83509	1.09417	-0.00034	0.984485  
>     12	0.684547	0.711437	1.1275	-0.00067	0.974748  
>     15	0.809017	0.587785	1.16183	0	1  
>     18	0.904827	0.416183	1.19722	-0.000717143	0.99192  

## Select values
 Comparison operators `<`, `<=`, `==`, `!=`,... allow to filter entries in reference and any input file according to values
 of any column. The option `--filter_epsilon` allows to set a tolerance (default is 0).
 Note the usage of `#` as a blackhole to ignore some columns.

     datamerge -f \
     	-r example/dataset_sampling_7.tsv 'time' '# time cosinusoidal<=0 exponential' \
     	-o example/dataset_selection.tsv 't' 'cosinusoidal exponential'

> Sample from output file [`example/dataset_selection.tsv`](example/dataset_selection.tsv):
>
>     t	cosinusoidal	exponential  
>     29	-0.24869	1.33643  
>     36	-0.637424	1.43333  

## Normalise data by value reached at some timepoint
 We now want to detect when one of the variables reaches a given threshold,
 and, for each column, use the corresponding entry to normalise all values in
 the dataset. This can be done by a combination of a test and a special
 interpolation called `{previous}`, which returns the latest entry that was
 matching the test.

     datamerge -f \
     	-r example/dataset_sampling_3.tsv 'time' \
     		'# time>0 sinusoidal exponential' \
     	-i example/dataset_sampling_3.tsv 'time_thresh{previous}' \
     		'# time_thresh sinusoidal_at_thresh exponential_thresh<1.1'	\
     	-o example/normalised.tsv 't' 'sinusoidal/sinusoidal_at_thresh time_thresh exponential/exponential_thresh' 

> Sample from output file [`example/normalised.tsv`](example/normalised.tsv):
>
>     t	sinusoidal/sinusoidal_at_thresh	time_thresh	exponential/exponential_thresh  
>     3	1	3	1  
>     6	1	6	1  
>     9	1	9	1  
>     12	1.27755	9	1.03046  
>     15	1.50985	9	1.06184  
>     18	1.68866	9	1.09418  
>     21	1.80764	9	1.1275  

 However, we may want to normalise also retrospectively the data that was present before the last entry matching the test.
 For this, we replace `time_thresh` by the value `0`: if `time` is always positive, then the `{previous}` interpolation operator
 will always return the last match of the test in the whole datafile.

     datamerge -f \
             -r example/dataset_sampling_3.tsv 'time' \
                     '# time>0 sinusoidal exponential' \
             -i example/dataset_sampling_3.tsv '0{previous}' \
                     '# time_thresh sinusoidal_at_thresh exponential_thresh<1.1'     \
             -o example/all_normalised.tsv 't' 'sinusoidal/sinusoidal_at_thresh time_thresh exponential/exponential_thresh'

> Sample from output file [`example/all_normalised.tsv`](example/all_normalised.tsv):
>
>     t	sinusoidal/sinusoidal_at_thresh	time_thresh	exponential/exponential_thresh  
>     3	0.349704	9	0.941764  
>     6	0.687022	9	0.970452  
>     9	1	9	1  
>     12	1.27755	9	1.03046  
>     15	1.50985	9	1.06184  
>     18	1.68866	9	1.09418  
>     21	1.80764	9	1.1275  

 :exclamation:  The result of the `{previous}` keyword is guaranteed only when using `C++-11`-compliant compilers. Checks are in place to verify this. 

## Multiple columns bulk management

 First, generate some data with `bash` if file (provided in the git) is not found

     NCOL=10
     if [ ! -e example/dataset_random.tsv ]
     then
         echo "# matrix of random numbers in [-16384,16383], $NMAX x $NCOL" > example/dataset_random.tsv
         for i in $(seq 1 $NMAX)
         do
           echo -n $i >> example/dataset_random.tsv
           for j in $(seq 1 $NCOL)
           do
     	echo -n " $(( RANDOM - 16384 ))" >> example/dataset_random.tsv
           done
           echo >> example/dataset_random.tsv
         done
     fi

> Sample from output file [`example/dataset_random.tsv`](example/dataset_random.tsv):
>
>     1 14515 -13947 -9374 11416 -4465 3765 -8106 -16254 2928 -3108  
>     2 -3077 6845 12574 -15978 1076 4092 6540 13772 -3862 220  
>     3 -766 5286 -6467 5837 11499 -13260 12398 8551 1606 -9385  
>     4 -16281 -16029 -4886 8031 -9717 1967 -8762 -1563 -10002 -16188  
>     5 6209 2312 13236 -246 -762 14199 -16364 3773 9980 -3778  
>     6 5849 -16127 -12378 13286 6628 12012 15346 13833 4776 11829  
>     7 -776 12290 4128 -15300 2412 -13894 14999 -14218 13606 -5545  

 Multiple columns bulk are managed with `variable[number]`, or, for output only, `variable[from:to]` or `variable[from:to:step]` syntaxes.
 Note here that `phase := line*0.1` can be used as a keyword in output columns.

     datamerge -f --constant MAX 32768. --constant SNR 5. \
     	-r example/dataset_random.tsv "line*0.1" "line r[10]" \
     	-o example/multicolum.tsv 'phase' "sin(phase)+r__1/(MAX*SNR) cos(phase)+r__0/(MAX*SNR) r[5] r[0:4]/MAX r[5:9]-r[9:5:-1]"

> Sample from output file [`example/multicolum.tsv`](example/multicolum.tsv):
>
>     phase	sin(phase)+r__1/(MAX*SNR)	cos(phase)+r__0/(MAX*SNR)	r__0	r__1	r__2	r__3	r__4	r__0/MAX	r__1/MAX	r__2/MAX	r__3/MAX	r__4/MAX	r__5-r__9	r__6-r__8	r__7-r__7	r__8-r__6	r__9-r__5  
>     0.1	0.0147077	1.0836	14515	-13947	-9374	11416	-4465	0.442963	-0.425629	-0.286072	0.348389	-0.136261	6873	-11034	0	11034	-6873  
>     0.2	0.240448	0.961286	-3077	6845	12574	-15978	1076	-0.0939026	0.208893	0.383728	-0.48761	0.0328369	3872	10402	0	-10402	-3872  
>     0.3	0.327783	0.950661	-766	5286	-6467	5837	11499	-0.0233765	0.161316	-0.197357	0.178131	0.350922	-3875	10792	0	-10792	3875  
>     0.4	0.291585	0.82169	-16281	-16029	-4886	8031	-9717	-0.496857	-0.489166	-0.149109	0.245087	-0.296539	18155	1240	0	-1240	-18155  
>     0.5	0.493537	0.915479	6209	2312	13236	-246	-762	0.189484	0.0705566	0.403931	-0.00750732	-0.0232544	17977	-26344	0	26344	-17977  
>     0.6	0.466211	0.861035	5849	-16127	-12378	13286	6628	0.178497	-0.492157	-0.377747	0.405457	0.202271	183	10570	0	-10570	-183  
>     0.7	0.71923	0.760106	-776	12290	4128	-15300	2412	-0.0236816	0.375061	0.125977	-0.466919	0.0736084	-8349	1393	0	-1393	8349  

 In `--input` or `--reference`, only the number of columns can be provided. In `--output`, one can use a range of values
 in the form `<first_index>:<last_index>`, and optionnally add a step size, `<first_index>:<last_index>:<step>`. One
 can also simply use a number `<n>` corresponding to the `n` first columns (`r[5]` in the example is equivalent to `r[0:4]`).
 Note that the table variables appear with double `_` and their index, and can be accessed in that way individually (`r__0`
 and `r__1` in the example).

 Some operations can be performed over a range of columns. E.g.:

     datamerge -f \
     	-r example/dataset_random.tsv 'line*0.1' 'line r[10]' \
     	-o example/dataset_sum.tsv t 'SUM{r[10]}/STAT_N{r[10]} SUM{r[1:3]}-(r__1+r__2+r__3)'

> Sample from output file [`example/dataset_sum.tsv`](example/dataset_sum.tsv):
>
>     t	SUM__0/STAT_N__1	SUM__2-(r__1+r__2+r__3)  
>     0.1	-2263	0  
>     0.2	2220.2	0  
>     0.3	1529.9	0  
>     0.4	-7343	0  
>     0.5	2855.9	0  
>     0.6	5505.4	0  
>     0.7	-229.8	0  

 Note the `{}` brackets. There is no possibility of nesting such operators, several runs of DataMerge are necessary
 if such operation is desired. If some values are not available, the result is `na` unless the `--cumulate_through`
 flag is set, in which case unavailable values are ignored.

     datamerge -f --cumulate_through \
     	-r example/dataset_random.tsv 'line*0.1' 'line r[10]' \
     	-o example/dataset_prod.tsv t 'STAT_N{r[999]} PROD{r[8:666]}-r__8*r__9' 

> Sample from output file [`example/dataset_prod.tsv`](example/dataset_prod.tsv):
>
>     t	STAT_N__0	PROD__1-r__8*r__9  
>     0.1	10	0  
>     0.2	10	0  
>     0.3	10	0  
>     0.4	10	0  
>     0.5	10	0  
>     0.6	10	0  
>     0.7	10	0  

 Available operators can be obtained with

     datamerge --dump_multicol_operators > example/list_of_operators.txt

> Sample from output file [`example/list_of_operators.txt`](example/list_of_operators.txt):
>
>     SUM PROD MEAN MEDIAN VAR STDDEV BIASED_VAR BIASED_STDDEV STAT_N   

 `STAT_N` returns the number of values which are not undefined.

## Multicolumn statistics the GiNaC way

 Some additional operations can be made on multiple columns *and* can be nested, namely: `min`, `max`, `mean` and `median`. Because of the way DataMerge handles 
 expressions (using the [GiNaC](https://www.ginac.de/) library), it is needed to specify the number of variables over which the operator is applied, and give
 each explicitely (no bulk processing with the `[]` operator is possible), e.g.: `max3(a,b,c)`. An additional operator, `stat_n`,
 returns the number of its arguments which have a value for the given entry, i.e. `stat_n3(a,b,c)` returns 3 if `a`, `b` and `c`
 have a value, or less than 3 if any or several of them are not available (`na`).

     datamerge -f \
     	-r example/linear_interpolation.tsv 't' \
     		't sinusoidal cosinusoidal exponential # #' \
     	-o example/statistics.tsv 'time' \
     		'min4(sinusoidal,cosinusoidal,exponential,1/exponential) max4(sinusoidal,cosinusoidal,exponential,1/exponential)  mean4(sinusoidal,cosinusoidal,exponential,1/exponential) stat_n4(sinusoidal,cosinusoidal,exponential,1/exponential)'

> Sample from output file [`example/statistics.tsv`](example/statistics.tsv):
>
>     time	min4(sinusoidal,cosinusoidal,exponential,1/exponential)	max4(sinusoidal,cosinusoidal,exponential,1/exponential)	mean4(sinusoidal,cosinusoidal,exponential,1/exponential)	stat_n4(sinusoidal,cosinusoidal,exponential,1/exponential)  
>     0	0	1	0.666667	3  
>     3	0.187381	1.03045	0.787883	4  
>     6	0.368125	1.06184	0.820703	4  
>     9	0.535827	1.09417	0.844755	4  
>     12	0.684547	1.1275	0.8526	4  
>     15	0.587785	1.16183	0.854836	4  
>     18	0.416183	1.19722	0.838375	4  

## Calculate finite differences
 Note multiple reading of the same data but with a time shift.

     datamerge -f \
     	--constant dt 3 \
     	-r example/dataset_sampling_3.tsv 'time_plus-dt/2' \
     		'# time_plus sinusoidal_plus exponential_plus' \
     	-i example/dataset_sampling_3.tsv 'time_minus+dt/2' \
     		'# time_minus sinusoidal_minus exponential_minus' \
     	-o example/dataset_finite_diff.tsv 'time' \
     		'(sinusoidal_plus-sinusoidal_minus)/(time_plus-time_minus) (exponential_plus-exponential_minus)/(time_plus-time_minus)'

> Sample from output file [`example/dataset_finite_diff.tsv`](example/dataset_finite_diff.tsv):
>
>     time	(sinusoidal_plus-sinusoidal_minus)/(time_plus-time_minus)	(exponential_plus-exponential_minus)/(time_plus-time_minus)  
>     -1.5	na	na  
>     1.5	0.0624603	0.01015  
>     4.5	0.060248	0.0104633  
>     7.5	0.0559007	0.0107767  
>     10.5	0.0495733	0.01111  
>     13.5	0.04149	0.0114433  
>     16.5	0.0319367	0.0117967  

 Note that the points which are not within the range of the grid they are interpolated from are not available (`na`), this is
 the case here with the first point shown, since there is no value at time `-3` that would allow to calculate the finite difference
 at `-1.5`. This point is shown because `time_plus-dt/2` evaluates to `-1.5` on the first line of the reference file. It is possible
 to filter it out:

     datamerge -f \
     	--constant dt 3 \
     	-r example/dataset_sampling_3.tsv 'time_plus-dt/2' \
     		'# time_plus>0 sinusoidal_plus exponential_plus' \
     	-i example/dataset_sampling_3.tsv 'time_minus+dt/2' \
     		'# time_minus sinusoidal_minus exponential_minus' \
     	-o example/dataset_finite_diff_filtered.tsv 'time' \
     		'(sinusoidal_plus-sinusoidal_minus)/(time_plus-time_minus) (exponential_plus-exponential_minus)/(time_plus-time_minus)'

> Sample from output file [`example/dataset_finite_diff_filtered.tsv`](example/dataset_finite_diff_filtered.tsv):
>
>     time	(sinusoidal_plus-sinusoidal_minus)/(time_plus-time_minus)	(exponential_plus-exponential_minus)/(time_plus-time_minus)  
>     1.5	0.0624603	0.01015  
>     4.5	0.060248	0.0104633  
>     7.5	0.0559007	0.0107767  
>     10.5	0.0495733	0.01111  
>     13.5	0.04149	0.0114433  
>     16.5	0.0319367	0.0117967  
>     19.5	0.021252	0.0121533  

## Cumulate values to calculate series, approximate integrals,...

 For this, use the symbols `+=` or `*=` in `--output` columns, which defines *cumulator* variables.

     datamerge -f --constant pi 3.14159265 \
     	-r example/dataset_sampling_3.tsv N "N" \
     	-o example/dataset_cumulate.tsv n "s+=n s s-n*(n-1)/2 factorial*=(n+1) factorial 1-sqrt(2*pi*n)*(n/exp(1))^n/factorial"

> Sample from output file [`example/dataset_cumulate.tsv`](example/dataset_cumulate.tsv):
>
>     n	s__+	s	s-n*(n-1)/2	factorial__+	factorial	1-sqrt(2*pi*n)*(n/exp(1))^n/factorial  
>     1	1	0	0	2	1	0.077863  
>     2	3	1	0	6	2	0.0404978  
>     3	6	3	0	24	6	0.0272984  
>     4	10	6	0	120	24	0.020576  
>     5	15	10	0	720	120	0.0165069  
>     6	21	15	0	5040	720	0.0137803  
>     7	28	21	0	40320	5040	0.0118262  

 Note that in the column where the "cumulator" variable is calculated, the value printed in a row is the one
 *after* calculation has taken place (left-hand side of column definition). In all other columns, the cumulator
 variable name can be called and contains the value *calculated in the previous row*. This is symbolised by the
 "__+" subscript.

 Additive cumulators are initialised to 0, multiplicative ones to 1. 
 If data is missing in a row and cannot be cumulated, the column is `na` after that row, unless the option `--cumulate_through` is set.

## Interpolate between datasets with LOESS 
 See [Wikipedia](https://en.wikipedia.org/wiki/Local_regression).
 Note that this is more appropriate for high-sampling large-noise data.
 Note also that this example shows that additional columns in file are ignored. 
 You probably have also noticed that column headers in the file are not used, but are rather
 always re-defined in the command line options.

     datamerge -f --bandwidth 0.8 --skip_header 1 \
     	-r example/dataset_sampling_3.tsv "2*acos(-1.)*time_sampling_3*${RATE}" \
     		'# time_sampling_3>0 sinusoidal' \
     	-i example/multicolum.tsv 'phase_input' \
     		'phase_input noisy_sin' \
     	-i example/multicolum.tsv 'phase_input_lsq{loess}' \
     		'phase_input_lsq noisy_sin_lsq' \
     	-o example/loess_interpolation.tsv 'phase_output' \
     		'sinusoidal noisy_sin_lsq noisy_sin ((noisy_sin_lsq-sinusoidal)/sinusoidal) ((noisy_sin-sinusoidal)/sinusoidal)' 

> Sample from output file [`example/loess_interpolation.tsv`](example/loess_interpolation.tsv):
>
>     phase_output	sinusoidal	noisy_sin_lsq	noisy_sin	((noisy_sin_lsq-sinusoidal)/sinusoidal)	((noisy_sin-sinusoidal)/sinusoidal)  
>     0.188496	0.187381	na	0.214478	na	0.144608  
>     0.376991	0.368125	na	0.299914	na	-0.185294  
>     0.565487	0.535827	0.505506	0.475642	-0.0565873	-0.112321  
>     0.753982	0.684547	0.655636	0.742564	-0.0422336	0.0847521  
>     0.942478	0.809017	0.779805	0.785185	-0.0361077	-0.029458  
>     1.13097	0.904827	0.852961	0.857381	-0.0573214	-0.0524363  
>     1.31947	0.968583	0.905356	0.883118	-0.0652783	-0.0882368  

 Note that the points for which the time-window of LOESS is incomplete are not available (`na`)

## Calculate derivative with LOESS
 A dash `'` can be used to denote the derivative of any variable that was read as a LOESS-interpolated input.
 Note that alternatively you can use the long format `_derivative`.
 In the example below the reference file is used only to provide the time grid.

     datamerge -f --bandwidth 0.8 \
     	-r example/multicolum.tsv 'time_orig' \
     		'time_orig' \
     	-i example/multicolum.tsv 'time_leastsquares{loess}' \
     		'time_leastsquares sinusoidal cosinusoidal' \
     	-o example/loess_derivation.tsv 'time' \
     		"sinusoidal sinusoidal' cosinusoidal -cosinusoidal_derivative abs(sinusoidal+cosinusoidal')"

> Sample from output file [`example/loess_derivation.tsv`](example/loess_derivation.tsv):
>
>     time	sinusoidal	sinusoidal'	cosinusoidal	-cosinusoidal'	abs(sinusoidal+cosinusoidal')  
>     0.1	na	na	na	na	na  
>     0.2	na	na	na	na	na  
>     0.3	na	na	na	na	na  
>     0.4	na	na	na	na	na  
>     0.5	na	na	na	na	na  
>     0.6	0.531515	0.776471	0.789072	0.635754	0.104239  
>     0.7	0.622671	0.896277	0.745341	0.584768	0.0379026  

## Non-regression test
 If differences are shown in file `example/diff` when you run this example,
 DataMerge may be broken. Check [doi: 10.5281/zenodo.5911337](https://dx.doi.org/10.5281/zenodo.5911337) 
 for the current version and recompile using an up-to-date C++-11 compliant
 compiler.  If problem persists, please contact the authors.

     echo Non-regression test fails if any difference is listed below > example/diff
     for LOC in example/*.tsv
     do
      REF=$(basename $LOC)
      diff -q $LOC example/reference_results/$REF >> example/diff
     done
     echo End of non-regression test >> example/diff

> Sample from output file [`example/diff`](example/diff):
>
>     Non-regression test fails if any difference is listed below  
>     End of non-regression test  

 I hope you enjoy DataMerge! Don't hesitate to share usage cases. Contributions welcome.
